package controller;

import javafx.stage.Stage;
import view.ViewManager;

public class Controller {

    private static Controller controller;
    private static ViewManager view;

    //************Constructor*************

    private Controller(Stage stage) {
        this.view = ViewManager.getViewManager(stage);

    }


    //***************Getters and setters**************

   public static Controller getController(Stage stage) {
        if (controller == null){
            controller = new Controller(stage);
        }
        return controller;
    }

    public static Controller getController(){
        return controller;
    }

    public static void setController(Controller controller) {
        Controller.controller = controller;
    }
}
