package model;

import view.UserView;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class DBUtil {

    private final String USER = "postgres";
    private final String PASSWORD = "1234";
    private final String CONNECTION = "jdbc:postgresql://localhost:5432/piljetter?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static DBUtil dbUtil;

    //*****Constructor********

    private DBUtil() {

    }

    //************Methods*************

    // creates a new user and gives 1000 pesetas unless username already exists.
   public void addUser(String userName){
        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement insertPs = conn.prepareStatement("INSERT INTO users (username, pesetas)" + "VALUES(?, ?)")
        ){
            insertPs.setString(1, userName);
            insertPs.setDouble(2, 1000);
            insertPs.executeUpdate();
            login(userName);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    // adds more pesetas to current user, prints the new balance.
    public void topUpBalance(String topUpString, String username){

        double topUp = Double.parseDouble(topUpString);

        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT pesetas, username FROM users WHERE username=?")
        ){
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            if(rs.next()){
                double currentBalance = rs.getDouble("pesetas");
                double newBalance = currentBalance + topUp;

                PreparedStatement preparedStatement = conn.prepareStatement("UPDATE users SET pesetas=? WHERE username= ?");
                preparedStatement.setDouble(1, newBalance);
                preparedStatement.setString(2, username);
                preparedStatement.executeUpdate();

                System.out.printf("New balance: %s", newBalance);
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    // checks if entered string matches band, venue, city, country or date.
    public void findConcert(String searchTerm){
        System.out.println("\n       SEARCH RESULT\n");
       try(
           Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
           PreparedStatement preparedStatement = conn.prepareStatement("SELECT band_name, concert_id, ticket_price, concert_date, venue_name FROM bands JOIN concerts ON bands.band_id=concerts.band_id JOIN venues ON venues.venue_id=concerts.venue_id WHERE band_name LIKE ? OR venue_name LIKE ? OR city LIKE ? OR country LIKE ? OR concert_date=?")
       ){
           preparedStatement.setString(1, "%" + searchTerm + "%"); // Band
           preparedStatement.setString(2, "%" + searchTerm + "%"); // Venue
           preparedStatement.setString(3, "%" + searchTerm + "%"); // City
           preparedStatement.setString(4, "%" + searchTerm + "%"); // Country
           try {
               java.util.Date enteredDate = new SimpleDateFormat("dd/MM/yy").parse(searchTerm);
               java.sql.Date sqlDate = new java.sql.Date(enteredDate.getTime());
               preparedStatement.setDate(5, sqlDate);
           }
           catch (ParseException e){
               java.util.Date defaultDate = new SimpleDateFormat("dd/MM/yy").parse("01/01/89");
               java.sql.Date sqlDate = new java.sql.Date(defaultDate.getTime());
               preparedStatement.setDate(5,sqlDate);
           }
           ResultSet bandRs = preparedStatement.executeQuery();

           while (bandRs.next()){
               String concert = bandRs.getString("concert_id");
               String band = bandRs.getString("band_name");
               String price = bandRs.getString("ticket_price");
               String venue = bandRs.getString("venue_name");
               java.sql.Date date = bandRs.getDate("concert_date");
               Date currentDate = new Date();

               if (date.compareTo(currentDate)>0) {

                   System.out.println("   Concert found:   ");
                   System.out.printf("   Id: %s", concert);
                   System.out.printf("   Band: %s", band);
                   System.out.printf("   Venue: %s", venue);
                   System.out.printf("   Date: %s", date);
                   System.out.printf("   Ticket price: %s", price);
                   System.out.println("\n");
               }
           }
       }
       catch (Exception e){
           System.err.println(e);
       }
    }

    // Checks if there are concerts between the two dates entered by the user.
    public void searchInterval(String date1, String date2) throws ParseException {

        java.util.Date enteredDate1 = new SimpleDateFormat("dd/MM/yy").parse(date1);
        java.sql.Date sqlDate1 = new java.sql.Date(enteredDate1.getTime());
        java.util.Date enteredDate2 = new SimpleDateFormat("dd/MM/yy").parse(date2);
        java.sql.Date sqlDate2 = new java.sql.Date(enteredDate2.getTime());

        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT band_name, concert_id, ticket_price, concert_date, venue_name FROM concerts JOIN bands ON concerts.band_id=bands.band_id JOIN venues ON concerts.venue_id=venues.venue_id WHERE concert_date BETWEEN ? AND ?")
        ){
            ps.setDate(1, sqlDate1);
            ps.setDate(2, sqlDate2);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                String concert = rs.getString("concert_id");
                String band = rs.getString("band_name");
                String price = rs.getString("ticket_price");
                String venue = rs.getString("venue_name");
                java.sql.Date date = rs.getDate("concert_date");

                System.out.println("   Concert found:   ");
                System.out.printf("   Id: %s", concert);
                System.out.printf("   Band: %s", band);
                System.out.printf("   Venue: %s", venue);
                System.out.printf("   Date: %s", date);
                System.out.printf("   Ticket price: %s", price);
                System.out.println("\n");
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    // Checks if the date has passed and there are still tickets left.
    private boolean checkAvailability(String concertString, String username, boolean voucherUsed){
            boolean purchaseSuccessful=false;
        try (
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT tickets_available, concert_date FROM concerts WHERE concert_id=?")
        ){
            UUID concertUUID = UUID.fromString(concertString);
            ps.setObject(1, concertUUID);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {

                int ticketsAvailable = rs.getInt("tickets_available");
                Date concertDate = rs.getDate("concert_date");
                Date currentDate = new Date();

                if (ticketsAvailable > 0) {
                    if (concertDate.compareTo(currentDate) > 0) {
                        buyTicket(concertString, username, voucherUsed);
                        ticketsAvailable -= 1;

                        PreparedStatement updatePs = conn.prepareStatement("UPDATE concerts SET tickets_available = ? WHERE concert_id=?");

                        updatePs.setInt(1, ticketsAvailable);
                        updatePs.setObject(2, concertUUID);
                        System.out.printf("Tickets left: %s", ticketsAvailable);
                        System.out.println("\n");
                        updatePs.executeUpdate();
                        purchaseSuccessful = true;
                    }
                    else{
                        System.out.println("Sorry, this concert already happened.");
                        System.out.println("\n");
                        purchaseSuccessful = false;
                    }
                }
                else {
                    System.out.println("Sorry, there are no tickets left.");
                    System.out.println("\n");
                    purchaseSuccessful = false;
                }
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
        return purchaseSuccessful;
    }

    // Gets the ticket price from the database, checks if user has that amount in their account. If true, calls checkAvailability to see if there are tickets, takes money from the user if true.
    public void checkBalance(String concertString, String username){
        boolean voucherUsed = false;
        try{
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            conn.setAutoCommit(false);
            PreparedStatement ticketPs = conn.prepareStatement("SELECT ticket_price FROM concerts WHERE concert_id=?");

            UUID concertId = UUID.fromString(concertString);
            ticketPs.setObject(1, concertId);
            ResultSet ticketRs = ticketPs.executeQuery();
            ticketRs.next();

            PreparedStatement userPs = conn.prepareStatement("SELECT pesetas FROM users WHERE username=?");
            userPs.setString(1, username);
            ResultSet userRs = userPs.executeQuery();

            if (userRs.next()){
                double balance = userRs.getDouble("pesetas");
                double ticketPrice = ticketRs.getDouble("ticket_price");
                System.out.println("let's check your balance");

                if (balance >= ticketPrice){
                    System.out.println("Money is enough");
                  boolean available = checkAvailability(concertString, username, false);

                  if(available) {
                      double newBalance = balance - ticketPrice;
                      PreparedStatement updatePs = conn.prepareStatement("UPDATE users SET pesetas = ? WHERE username=?");
                      updatePs.setDouble(1, newBalance);
                      updatePs.setString(2, username);

                      System.out.printf("User %s", username);
                      System.out.printf(" now has %s", newBalance);
                      System.out.println("\n");
                      updatePs.executeUpdate();
                      conn.commit();
                      updatePs.close();
                  }
                  else{
                    System.out.println("Transaction canceled, no tickets available");
                  }
                }
                else{
                    System.out.println("You don't have enough pesetas");
                    System.out.println("\n");
                }
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    // Checks if the user has a voucher that hasn't expired yet, deletes expired vouchers. Calls checkAvailability if voucher is valid, removes the used voucher if transaction was successful.
    public void checkVouchers(String concertString, String username){
        boolean voucherValid = false, purchaseSuccessful=false, voucherUsed=true;

        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT voucher_id FROM vouchers WHERE username=?")
        ){
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                System.out.println("There's a voucher!");

                PreparedStatement expiryPs = conn.prepareStatement("SELECT voucher_id, expiry_date FROM vouchers WHERE username=?");
                expiryPs.setString(1, username);
                ResultSet expiryRs = expiryPs.executeQuery();

                while (expiryRs.next()){
                    Date expiryDate = expiryRs.getDate("expiry_date");
                    Date currentDate = new Date();
                    java.sql.Date sqlCurrentDate = new java.sql.Date(currentDate.getTime());

                    if (expiryDate.compareTo(sqlCurrentDate)>0){
                        System.out.println("Voucher is still valid!");
                        voucherValid = true;
                            purchaseSuccessful = checkAvailability(concertString, username, true);

                            if(purchaseSuccessful) {

                                Object voucherID = rs.getObject("voucher_id");
                                PreparedStatement psRemove = conn.prepareStatement("DELETE FROM vouchers WHERE voucher_id=?");
                                psRemove.setObject(1, voucherID);
                                psRemove.execute();
                                psRemove.close();

                                System.out.printf("Voucher removed: %s", voucherID);
                                break;
                            }
                        }
                    else {
                        System.out.println("This voucher has expired!");
                        voucherValid = false;

                        Object voucherId = expiryRs.getObject("voucher_id");
                        PreparedStatement removeExpiredVoucher = conn.prepareStatement("DELETE FROM vouchers WHERE voucher_id=?");
                        removeExpiredVoucher.setObject(1, voucherId);
                        removeExpiredVoucher.execute();
                        removeExpiredVoucher.close();
                    }
                }
            }
        }
        catch(Exception e){
            System.err.println(e);
        }
    }

    // Registers the purchased ticket to the database and prints it.
    private void buyTicket(String concertString, String username, boolean voucherUsed){

        try (
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("INSERT INTO tickets (ticket_id, concert_id, username, date_purchased, voucher_used)" + "VALUES(?, ?, ?, ?, ?)")
        ){
            UUID ticketId = UUID.randomUUID();
            UUID concertId = UUID.fromString(concertString);
            Date currentDate = new Date();
            java.sql.Date sqlDate = new java.sql.Date(currentDate.getTime());

            ps.setObject(1, ticketId);
            ps.setObject(2, concertId);
            ps.setString(3, username);
            ps.setDate(4, sqlDate);
            ps.setBoolean(5, voucherUsed);
            ps.execute();
            ps.close();

            PreparedStatement printTicket = conn.prepareStatement("SELECT tickets.ticket_id, bands.band_name, venues.venue_name, concerts.ticket_price, concerts.concert_date, tickets.date_purchased, tickets.username FROM tickets INNER JOIN concerts ON tickets.concert_id=concerts.concert_id INNER JOIN bands ON concerts.band_id=bands.band_id INNER JOIN venues ON concerts.venue_id=venues.venue_id WHERE tickets.ticket_id=?");
            printTicket.setObject(1, ticketId);
            ResultSet printRs = printTicket.executeQuery();

            if (printRs.next()){
                System.out.println("Here's your ticket: \n");
            String ticket = printRs.getString("ticket_id");
            String bandName = printRs.getString("band_name");
            String venueName = printRs.getString("venue_name");
            String ticketPrice = printRs.getString("ticket_price");
            String concertDate = printRs.getString("concert_date");
            String datePurchased = printRs.getString("date_purchased");
            String user = printRs.getString("username");

            System.out.printf("   Ticket: %s", ticket);
            System.out.printf("   Band: %s", bandName);
            System.out.printf("   Venue: %s", venueName);
            System.out.printf("   Concert date: %s", concertDate);
            System.out.printf("   Date purchased: %s", datePurchased);
            System.out.printf("   User: %s", user);
            if (voucherUsed) {
                System.out.println("   Voucher used");
            } else {
                System.out.printf("   Price: %s", ticketPrice);
            }
            System.out.println("\n");
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    // Shows user all relevant information about the tickets they've bought.
    public void printTickets(String username){
        System.out.println("   Your tickets: \n");
        try (
            Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = connection.prepareStatement("SELECT tickets.ticket_id, bands.band_name, venues.venue_name, concerts.ticket_price, concerts.concert_date, tickets.date_purchased, tickets.voucher_used FROM tickets JOIN concerts ON concerts.concert_id=tickets.concert_id JOIN bands ON concerts.band_id=bands.band_id INNER JOIN venues ON concerts.venue_id=venues.venue_id WHERE username= ?")
        ){
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                String ticketId = rs.getString("ticket_id");
                String bandName = rs.getString("band_name");
                String venueName = rs.getString("venue_name");
                String ticketPrice = rs.getString("ticket_price");
                String concertDate = rs.getString("concert_date");
                String datePurchased = rs.getString("date_purchased");
                boolean voucherUsed = rs.getBoolean("voucher_used");

                System.out.printf("   Ticket: %s", ticketId);
                System.out.printf("   Band: %s", bandName);
                System.out.printf("   Venue: %s", venueName);
                System.out.printf("   Concert date: %s", concertDate);
                System.out.printf("   Date purchased: %s", datePurchased);

                if (voucherUsed){
                    System.out.println("   Voucher used");
                }
                else {
                    System.out.printf("   Price: %s", ticketPrice);
                }
                System.out.println("\n");
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    // Shows all vouchers if admin is logged in, or vouchers that belongs to current user.
    public void printVouchers(String username){
        System.out.println("\n");
        System.out.println("   Vouchers issued: \n");
        try (
            Connection connection = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = connection.prepareStatement("SELECT voucher_id, expiry_date FROM vouchers WHERE username= ?")
        ){
            if (username.equals("admin")){
                PreparedStatement adminPs = connection.prepareStatement("SELECT * FROM vouchers");
                ResultSet rs = adminPs.executeQuery();

                while (rs.next()) {
                    String voucherId = rs.getString("voucher_id");
                    String expiryDate = rs.getString("expiry_date");
                    String users = rs.getString("username");

                    System.out.printf("   Voucher: %s", voucherId);
                    System.out.printf("   Expiry date: %s", expiryDate);
                    System.out.printf("   User: %s", users);
                    System.out.println("\n");
                }
            }
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                String voucherId = rs.getString("voucher_id");
                String expiryDate = rs.getString("expiry_date");

                System.out.printf("   Voucher: %s", voucherId);
                System.out.printf("   Expiry date: %s", expiryDate);
                System.out.println("\n");
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    // Identifies concert to cancel and removes it from the database.
    private void cancelConcert(String concertString){

        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT concert_id FROM concerts WHERE concert_id = ?")
        ){
            UUID concertUUID = UUID.fromString(concertString);
            ps.setObject(1, concertUUID);
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                Object concert = rs.getObject("concert_id");
                PreparedStatement deletePs = conn.prepareStatement("DELETE FROM concerts WHERE concert_id=?");
                deletePs.setObject(1, concert);

                String printConcert = rs.getString("concert_id");
                System.out.printf("   Cancelled concert: %s", printConcert);
                System.out.println("\n");
            }
            ps.execute();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    // Finds all tickets for cancelled concert and gives the customers their pesetas back.
    public void refund(String concertString){

        try (
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT concerts.concert_id, concerts.ticket_price, tickets.ticket_id, tickets.username, users.pesetas FROM concerts INNER JOIN tickets ON concerts.concert_id=tickets.concert_id INNER JOIN users ON tickets.username=users.username WHERE concerts.concert_id=?")
        ){
            UUID concertUUID = UUID.fromString(concertString);
            preparedStatement.setObject(1, concertUUID);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {

                PreparedStatement ps = conn.prepareStatement("UPDATE users SET pesetas = ? WHERE username=?");
                Object ticketId = rs.getObject("ticket_id");
                double ticketPrice = rs.getDouble("ticket_price");
                String username = rs.getString("username");
                double pesetas = rs.getDouble("pesetas");
                double newPesetas = pesetas + ticketPrice;

                ps.setDouble(1, newPesetas);
                ps.setString(2, username);

                System.out.printf("\n   Concert id: %s", concertUUID);
                System.out.printf("   Ticket price: %s", ticketPrice);
                System.out.printf("   Tickets: %s", ticketId);
                System.out.printf("   Belongs to: %s", username);
                System.out.printf("   User's current balance: %s", pesetas);
                System.out.printf("   User's new balance: %s", newPesetas);
                System.out.println("\n");

                ps.executeUpdate();
                ps.close();
            }
            preparedStatement.execute();
            preparedStatement.close();

            addVoucher(concertString);
            cancelConcert(concertString);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    // Finds all tickets for the cancelled concert, gives owner of each ticket a voucher and sets the expiry date to 1 year in the future.
    public void addVoucher(String concertString){
        try(

            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT concerts.concert_id, tickets.ticket_id, users.username FROM concerts INNER JOIN tickets ON concerts.concert_id=tickets.concert_id INNER JOIN users ON tickets.username=users.username WHERE concerts.concert_id=?");
        ){
            UUID concertId = UUID.fromString(concertString);

            ps.setObject(1, concertId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){

                PreparedStatement insertPs = conn.prepareStatement("INSERT INTO vouchers (voucher_id, expiry_date, username)" + "VALUES(?,?,?)");

                UUID voucherId = UUID.randomUUID();
                Date currentDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(currentDate);
                c.add(Calendar.DATE,365);
                Date expiryDate = c.getTime();
                java.sql.Date sqlExpiryDate = new java.sql.Date(expiryDate.getTime());
                String username = rs.getString("username");

                insertPs.setObject(1, voucherId);
                insertPs.setDate(2, sqlExpiryDate);
                insertPs.setString(3, username);

                System.out.printf("   Voucher id: %s", voucherId);
                System.out.printf("   Expiry date: %s", sqlExpiryDate);
                System.out.printf("   User: %s", username);
                System.out.println("\n");
                insertPs.executeUpdate();
            }

            ps.execute();
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    // Checks is user exists and logs in if true.
    public String login(String username){
        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT username FROM users WHERE username = ?")
        ){
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                String user = rs.getString("username");
                System.out.println("User " + user + " signed in");
                UserView.getUserView(username);

            }
            else{
                System.out.println("User " + username + " does not exist, please sign up");
            }
        }
        catch(Exception e){
            System.out.println(e);
        }

        return username;
    }

    // Checks how much pesetas current user has.
    public double fetchBalance(String username){
        try (
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT pesetas FROM users WHERE username = ?")
        ){
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getDouble("pesetas");
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
        return 0;
    }

    // Shows how much each band sold within a specific time frame.
    public void bandIncome(String date1, String date2) throws ParseException {
        System.out.println("\n");
        System.out.println("   Income by band: \n");

        java.util.Date enteredDate1 = new SimpleDateFormat("dd/MM/yy").parse(date1);
        java.sql.Date sqlDate1 = new java.sql.Date(enteredDate1.getTime());
        java.util.Date enteredDate2 = new SimpleDateFormat("dd/MM/yy").parse(date2);
        java.sql.Date sqlDate2 = new java.sql.Date(enteredDate2.getTime());

        try(

            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement bandsPS = conn.prepareStatement("SELECT DISTINCT concerts.band_id, bands.band_name FROM concerts JOIN bands ON concerts.band_id=bands.band_id JOIN tickets ON concerts.concert_id=tickets.concert_id WHERE date_purchased BETWEEN ? AND ?")
        )
        {
            bandsPS.setObject(1, sqlDate1);
            bandsPS.setObject(2, sqlDate2);
            ResultSet bandsRs = bandsPS.executeQuery();

            while(bandsRs.next()) {
                String bandId = bandsRs.getString("band_id");
                String bandName = bandsRs.getString("band_name");
                System.out.printf("   Band: %s", bandName);


                // Checks pesetas earned by a specific band.
                PreparedStatement bandTicketsPs = conn.prepareStatement("SELECT SUM(ticket_price) FROM concerts JOIN tickets ON concerts.concert_id=tickets.concert_id WHERE band_id=? AND date_purchased BETWEEN ? AND ?");
                UUID bandUUID = UUID.fromString(bandId);
                bandTicketsPs.setObject(1, bandUUID);
                bandTicketsPs.setDate(2, sqlDate1);
                bandTicketsPs.setDate(3, sqlDate2);
                ResultSet ticketsRs = bandTicketsPs.executeQuery();

                while (ticketsRs.next()){
                    double pesetas = ticketsRs.getDouble(1);
                    System.out.printf("   Pesetas earned: %s", pesetas);
                    System.out.println("\n");
                }
            }

        }
        catch(Exception e){
            System.err.println(e);
        }
    }

    // Shows number of tickets sold and pesetas earned within date interval entered by user.
    public void showIncome(String date1, String date2) throws ParseException {
        System.out.println("\n");

        java.util.Date enteredDate1 = new SimpleDateFormat("dd/MM/yy").parse(date1);
        java.sql.Date sqlDate1 = new java.sql.Date(enteredDate1.getTime());
        java.util.Date enteredDate2 = new SimpleDateFormat("dd/MM/yy").parse(date2);
        java.sql.Date sqlDate2 = new java.sql.Date(enteredDate2.getTime());

        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("SELECT SUM(ticket_price) FROM concerts JOIN tickets ON concerts.concert_id=tickets.concert_id WHERE date_purchased BETWEEN ? AND ?")
        )
        {
            ps.setObject(1, sqlDate1);
            ps.setObject(2, sqlDate2);
            ResultSet rs = ps.executeQuery();

            if (rs.next()){
                double totalIncome = rs.getDouble(1);
                System.out.printf("     Income in pesetas: %s", totalIncome);
            }

            PreparedStatement amountPs = conn.prepareStatement("SELECT COUNT(ticket_id) FROM tickets JOIN concerts ON concerts.concert_id=tickets.concert_id WHERE date_purchased BETWEEN ? AND ?");

            amountPs.setObject(1, sqlDate1);
            amountPs.setObject(2, sqlDate2);
            ResultSet amountRs = amountPs.executeQuery();

            if (amountRs.next()){
                int amount = amountRs.getInt(1);
                System.out.printf("     Number of tickets sold: %s",  amount);
            }
        }
        catch(Exception e){
            System.err.println(e);
        }
    }

    // Adds a new concert unless band or venue is busy on the proposed date.
   public void registerNewConcert(String bandName, String venueName, String dateString, String ticketString, String ticketsAvailableString){

        try(
            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement bandPs = conn.prepareStatement("SELECT band_id, band_popularity FROM bands WHERE band_name=?")
        ){
            bandPs.setString(1, bandName);
            PreparedStatement venuePs = conn.prepareStatement("SELECT venue_id, venue_popularity FROM venues WHERE venue_name=?");
            venuePs.setString(1, venueName);
            ResultSet bandRs = bandPs.executeQuery();
            ResultSet venueRs = venuePs.executeQuery();
            venueRs.next();

                if (bandRs.next()) {

                        PreparedStatement ps = conn.prepareStatement("INSERT INTO concerts (concert_id, band_id, venue_id, concert_date, ticket_price, tickets_available, organizer_cost)" + "VALUES(?, ?, ?, ?, ?, ?, ?)");

                        UUID concertId = UUID.randomUUID();
                        Object bandId = bandRs.getObject("band_id");
                        Object venueId = venueRs.getObject("venue_id");
                        java.util.Date enteredDate = new SimpleDateFormat("dd/MM/yy").parse(dateString);
                        java.sql.Date sqlDate = new java.sql.Date(enteredDate.getTime());
                        double ticketPrice = Double.parseDouble(ticketString);
                        int ticketsAvailableInt = Integer.parseInt(ticketsAvailableString);
                        int bandPopularity = bandRs.getInt("band_popularity");
                        int venuePopularity = venueRs.getInt("venue_popularity");
                        double organizerCost = (bandPopularity * venuePopularity) * 1000;
                        System.out.println("Organizer cost: " + organizerCost);

                        ps.setObject(1, concertId);
                        ps.setObject(2, bandId);
                        ps.setObject(3, venueId);
                        ps.setDate(4, sqlDate);
                        ps.setDouble(5, ticketPrice);
                        ps.setInt(6, ticketsAvailableInt);
                        ps.setDouble(7, organizerCost);
                        ps.executeUpdate();
            }
        }
        catch (Exception e){
            System.err.println(e);
        }
   }

   // Adds a new band to the database.
   public void registerNewBand(String bandStringName, String bandPopularityString) {
       try (
               Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
               PreparedStatement ps = conn.prepareStatement("INSERT INTO bands (band_id, band_name, band_popularity)" + "VALUES(?, ?, ?)")
       ){
            UUID bandId = UUID.randomUUID();
            int bandPopularity = Integer.parseInt(bandPopularityString);

            ps.setObject(1, bandId);
            ps.setObject(2, bandStringName);
            ps.setObject(3, bandPopularity);
            ps.executeUpdate();

            System.out.println("     Venue registered:");
            System.out.printf("   Name: %s", bandStringName);
            System.out.printf("   Popularity: %s", bandPopularity);
        }
       catch (Exception r) {
            System.err.println(r);
        }
   }

   // Adds a new venue to the database.
   public void registerNewVenue(String venueName, String city, String country, String venuePopularityString){
        try (

            Connection conn = DriverManager.getConnection(CONNECTION, USER, PASSWORD);
            PreparedStatement ps = conn.prepareStatement("INSERT INTO venues (venue_id, venue_name, city, country, venue_popularity)" + "VALUES(?, ?, ?, ?, ?)")
            )
            {
            UUID venueId = UUID.randomUUID();
            int venuePopularity = Integer.parseInt(venuePopularityString);

            ps.setObject(1, venueId);
            ps.setObject(2, venueName);
            ps.setObject(3, city);
            ps.setObject(4, country);
            ps.setObject(5, venuePopularity);

            ps.executeUpdate();

                System.out.println("     Venue registered:");
                System.out.printf("   Name: %s", venueName);
                System.out.printf("   City: %s", city);
                System.out.printf("   Country: %s", country);
                System.out.printf("   Popularity: %s", venuePopularity);

        }
        catch (Exception t){
            System.err.println(t);
        }

   }

    //*********Getters and setters*************

    public static DBUtil getDbUtil() {
       if (dbUtil == null){
           dbUtil = new DBUtil();
       }
        return dbUtil;
    }
}
