package view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.DBUtil;

import java.text.ParseException;

public class AdminView implements IState{
    private static Scene adminScene;
    private static AnchorPane adminPane;
    private static AdminView adminView;
    private static Stage mainStage;

    AdminView() {
        adminPane = new AnchorPane();
        adminScene = new Scene(adminPane, 900, 600);
        ViewManager.getViewManager(mainStage).setMainScene(adminScene);
        initializeAdminPane();
    }

    private void initializeAdminPane() {

        Text registerText = new Text("Register new concert");
        registerText.setFont(Font.font(25));
        registerText.setX(20);
        registerText.setY(80);


        Text bandText = new Text("Enter band name");
        bandText.setX(20);
        bandText.setY(145);

        TextField bandField = new TextField();
        bandField.setLayoutX(200);
        bandField.setLayoutY(130);


        Text venueText = new Text("Enter venue name");
        venueText.setX(20);
        venueText.setY(185);

        TextField venueField = new TextField();
        venueField.setLayoutX(200);
        venueField.setLayoutY(170);


        Text dateText = new Text("Enter date (dd/mm/yy)");
        dateText.setX(20);
        dateText.setY(225);

        TextField dateField = new TextField();
        dateField.setLayoutX(200);
        dateField.setLayoutY(210);


        Text ticketPriceText = new Text("Enter ticket price");
        ticketPriceText.setX(20);
        ticketPriceText.setY(265);

        TextField ticketPriceField = new TextField();
        ticketPriceField.setLayoutX(200);
        ticketPriceField.setLayoutY(250);

        Text capacityText = new Text("Enter ticket quantity");
        capacityText.setX(20);
        capacityText.setY(310);

        TextField capacityField = new TextField();
        capacityField.setLayoutX(200);
        capacityField.setLayoutY(290);

        Button registerButton = new Button("Register");
        registerButton.setLayoutX(100);
        registerButton.setLayoutY(350);

        Text cancelText = new Text("Cancel concert");
        cancelText.setFont(Font.font(25));
        cancelText.setLayoutX(20);
        cancelText.setLayoutY(440);

        Text cancelInstruction = new Text("Enter concert id to cancel");
        cancelInstruction.setLayoutX(20);
        cancelInstruction.setLayoutY(485);

        TextField cancelField = new TextField();
        cancelField.setLayoutX(200);
        cancelField.setLayoutY(470);

        Button cancelButton = new Button("Cancel concert");
        cancelButton.setLayoutX(20);
        cancelButton.setLayoutY(520);

        Text addBandText = new Text("Add new band");
        addBandText.setFont(Font.font(25));
        addBandText.setLayoutX(450);
        addBandText.setLayoutY(80);

        Text bandNameText = new Text("Enter name of the band");
        bandNameText.setLayoutX(450);
        bandNameText.setLayoutY(140);

        TextField bandNameTextField = new TextField();
        bandNameTextField.setLayoutX(640);
        bandNameTextField.setLayoutY(130);

        Text bandPopularity = new Text("Enter the popularity of the band");
        bandPopularity.setLayoutX(450);
        bandPopularity.setLayoutY(185);

        TextField popularityTextField = new TextField();
        popularityTextField.setLayoutX(640);
        popularityTextField.setLayoutY(170);

        Button addBand = new Button("Add band");
        addBand.setLayoutX(450);
        addBand.setLayoutY(220);

        Button viewStatistics = new Button("View all statistics");
        viewStatistics.setLayoutX(15);
        viewStatistics.setLayoutY(10);

        Button viewBandIncome = new Button("Band ticket sales");
        viewBandIncome.setLayoutX(125);
        viewBandIncome.setLayoutY(10);

        Button viewIncome = new Button("Total income");
        viewIncome.setLayoutX(235);
        viewIncome.setLayoutY(10);

        Button viewVouchers = new Button("Issued vouchers");
        viewVouchers.setLayoutX(325);
        viewVouchers.setLayoutY(10);

        Text dates = new Text("Enter dates (dd/mm/yy)");
        dates.setFont(Font.font(12));
        dates.setLayoutX(430);
        dates.setLayoutY(28);

        TextField date1 = new TextField();
        date1.setLayoutX(570);
        date1.setLayoutY(10);

        TextField date2 = new TextField();
        date2.setLayoutX(720);
        date2.setLayoutY(10);

        Text venue = new Text("Add new venue");
        venue.setFont(Font.font(25));
        venue.setLayoutX(450);
        venue.setLayoutY(300);

        Text venueName = new Text("Enter the name of the venue");
        venueName.setLayoutX(450);
        venueName.setLayoutY(350);

        TextField venueNameTextField = new TextField();
        venueNameTextField.setLayoutX(640);
        venueNameTextField.setLayoutY(340);

        Text cityText = new Text("Enter City");
        cityText.setLayoutX(450);
        cityText.setLayoutY(390);

        TextField cityTextField = new TextField();
        cityTextField.setLayoutX(640);
        cityTextField.setLayoutY(380);

        Text countryText = new Text("Enter Country");
        countryText.setLayoutX(450);
        countryText.setLayoutY(430);

        TextField countryTextField = new TextField();
        countryTextField.setLayoutX(640);
        countryTextField.setLayoutY(420);

        Text venuePopularity = new Text("Enter the popularity of the venue");
        venuePopularity.setLayoutX(450);
        venuePopularity.setLayoutY(475);

        TextField venuePopularityField = new TextField();
        venuePopularityField.setLayoutX(640);
        venuePopularityField.setLayoutY(460);

        Button addVenue = new Button("Add venue");
        addVenue.setLayoutX(450);
        addVenue.setLayoutY(500);

        adminPane.getChildren().addAll(
                registerText, bandText, venueText, dateText, ticketPriceText, capacityText, bandField,
                venueField, dateField, ticketPriceField, capacityField, registerButton, cancelField,
                cancelButton, cancelText, cancelInstruction, addBandText, bandNameText, bandNameTextField, bandPopularity,
                popularityTextField, addBand, venue, venueName, venueNameTextField, cityText, cityTextField, countryText, countryTextField,
                venuePopularity, venuePopularityField, addVenue, viewStatistics, viewBandIncome, viewIncome, viewVouchers, dates, date1, date2);

        registerButton.setOnAction(event -> DBUtil.getDbUtil().registerNewConcert(bandField.getText(), venueField.getText(), dateField.getText(), ticketPriceField.getText(), capacityField.getText()));
        cancelButton.setOnAction(event -> DBUtil.getDbUtil().refund(cancelField.getText()));
        addBand.setOnAction(event -> DBUtil.getDbUtil().registerNewBand(bandNameTextField.getText(), popularityTextField.getText()));
        addVenue.setOnAction(event -> DBUtil.getDbUtil().registerNewVenue(venueNameTextField.getText(), cityTextField.getText(), countryTextField.getText(), venuePopularityField.getText()));

        viewVouchers.setOnAction(event -> DBUtil.getDbUtil().printVouchers("admin"));
        viewBandIncome.setOnAction(event -> {
            try {
                String date1String = date1.getText(), date2String = date2.getText();
                if(date1String.isEmpty() || date2String.isEmpty()) {
                    System.out.println("Please enter date interval");
                }
                else{
                    DBUtil.getDbUtil().bandIncome(date1String, date2String);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        viewIncome.setOnAction(event -> {
            try {
                String date1String = date1.getText(), date2String = date2.getText();
                if(date1String.isEmpty() || date2String.isEmpty()) {
                    System.out.println("Please enter date interval");
                }
                else {
                    DBUtil.getDbUtil().showIncome(date1.getText(), date2.getText());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
        viewStatistics.setOnAction(event -> {
            try {
                String date1String = date1.getText(), date2String = date2.getText();
                if(date1String.isEmpty() || date2String.isEmpty()) {
                    System.out.println("Please enter date interval");
                }
                else{
                    DBUtil.getDbUtil().showIncome(date1String, date2String);
                    DBUtil.getDbUtil().printVouchers("admin");
                    DBUtil.getDbUtil().bandIncome(date1String, date2String);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    //************Getters and setters******************

    public static AdminView getAdminView() {
        return adminView;
    }
}
