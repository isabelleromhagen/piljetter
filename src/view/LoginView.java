package view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.DBUtil;

public class LoginView implements IState {
    private static Scene loginScene;
    private static AnchorPane loginPane;
    private static LoginView loginView;
    private static Stage mainStage;

//***************Constructor********************

    private LoginView() {
        loginPane = new AnchorPane();
        loginScene = new Scene(loginPane, 500, 600);
        initializeLoginPane();
    }

    //***************Methods************************

    private void initializeLoginPane(){

        Text welcomeText = new Text("Piljetter.se");
        welcomeText.setFont(Font.font(25));
        welcomeText.setLayoutX(190);
        welcomeText.setLayoutY(80);

        Text enterUserText = new Text("Enter your username to login");
        enterUserText.setX(170);
        enterUserText.setY(140);

        TextField enterUsername = new TextField();
        enterUsername.setLayoutX(170);
        enterUsername.setLayoutY(200);

        Button loginButton = new Button();
        loginButton.setText("Login");
        loginButton.setPrefWidth(50);
        loginButton.setLayoutX(220);
        loginButton.setLayoutY(240);

        loginButton.setOnAction(event -> DBUtil.getDbUtil().login(enterUsername.getText()));

        Button signUpButton = new Button();
        signUpButton.setText("Sign up");
        signUpButton.setPrefWidth(80);
        signUpButton.setLayoutX(205);
        signUpButton.setLayoutY(280);
        signUpButton.setOnAction(event -> new SignUpView());

        Button adminLoginButton = new Button();
        adminLoginButton.setText("Admin");
        adminLoginButton.setPrefWidth(80);
        adminLoginButton.setLayoutX(205);
        adminLoginButton.setLayoutY(360);
        adminLoginButton.setOnAction(event -> {
            try {
                new AdminView();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        loginPane.getChildren().addAll(welcomeText, enterUserText, enterUsername, loginButton, signUpButton, adminLoginButton);
    }


    //***********Getters and setters*****************

    static Scene getLoginScene() {
        return loginScene;
    }

    static LoginView getLoginView() {
        if(loginView == null){
            loginView = new LoginView();
        }
        return loginView;
    }

    public static AnchorPane getLoginPane() {
        return loginPane;
    }
}
