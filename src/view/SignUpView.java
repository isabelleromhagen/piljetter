package view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.DBUtil;


public class SignUpView implements IState {
    private static Scene signUpScene;
    private static AnchorPane signUpPane;
    private static SignUpView signUpView;
    private static Stage mainStage;
    private static LoginView loginView;

//***************Constructor********************

    SignUpView() {
        signUpPane = new AnchorPane();
        signUpScene = new Scene(signUpPane, 500, 600);
        ViewManager.getViewManager(mainStage).setMainScene(signUpScene);
        initializeSignUpPane();
    }

    //***************Methods************************

    private void initializeSignUpPane() {

        Text title = new Text("Create account");
        title.setFont(Font.font(25));
        title.setLayoutX(160);
        title.setLayoutY(80);

        Text text = new Text("Enter your username to sign up");
        text.setX(170);
        text.setY(140);

        TextField signUpUsername = new TextField();
        signUpUsername.setLayoutX(170);
        signUpUsername.setLayoutY(160);

        Button registerButton = new Button();
        registerButton.setText("Sign up");
        registerButton.setPrefWidth(80);
        registerButton.setLayoutX(200);
        registerButton.setLayoutY(220);
        registerButton.setOnAction(event -> DBUtil.getDbUtil().addUser(signUpUsername.getText()));

        signUpPane.getChildren().addAll(title, text, signUpUsername, registerButton);
    }


    //***********Getters and setters*****************


    public static Scene getSignUpScene() {
        return signUpScene;
    }

    public static AnchorPane getSignUpPane() {
        return signUpPane;
    }

    public static SignUpView getSignUpView() {
        if (signUpView == null){
            signUpView = new SignUpView();
        }
        return signUpView;
    }
}

