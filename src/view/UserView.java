package view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.DBUtil;

import java.text.ParseException;

public class UserView implements IState{

        private static Scene userScene;
        private static AnchorPane userPane;
        private static UserView userView;
        private static Stage mainStage;

//***************Constructor********************

        private UserView(String userName) {
            userPane = new AnchorPane();
            userScene = new Scene(userPane, 500, 600);
            ViewManager.getViewManager(mainStage).setMainScene(userScene);
            double balance = DBUtil.getDbUtil().fetchBalance(userName);
            initializeUserPane(userName, balance);

        }

        //***************Methods************************

        private void initializeUserPane(String userName, double balance){
            Text text = new Text("Welcome to your page");
            text.setFont(Font.font(25));
            text.setX(20);
            text.setY(30);

            Text balanceText = new Text("Current balance: " + balance + " pesetas");
            balanceText.setFont(Font.font(14));
            balanceText.setX(25);
            balanceText.setY(80);

            Text refillPesetas = new Text("Buy more Pesetas");
            refillPesetas.setLayoutX(25);
            refillPesetas.setLayoutY(100);

            TextField topUpField = new TextField();
            topUpField.setLayoutX(20);
            topUpField.setLayoutY(120);

            Button topUpButton = new Button("Buy");
            topUpButton.setLayoutX(180);
            topUpButton.setLayoutY(120);

            Text searchText = new Text("Find concerts");
            searchText.setFont(Font.font(18));
            searchText.setX(30);
            searchText.setY(210);

            Text dateFormat = new Text("(dd/mm/yy) , use both fields to enter a date interval");
            dateFormat.setLayoutX(200);
            dateFormat.setLayoutY(210);

            TextField searchField = new TextField();
            searchField.setLayoutX(20);
            searchField.setLayoutY(230);

            TextField searchField2 = new TextField();
            searchField2.setLayoutX(180);
            searchField2.setLayoutY(230);

            Button searchButton = new Button();
            searchButton.setText("Search");
            searchButton.setPrefWidth(80);
            searchButton.setLayoutX(90);
            searchButton.setLayoutY(270);

            Button searchButton2 = new Button();
            searchButton2.setText("Search date interval");
            searchButton2.setPrefWidth(140);
            searchButton2.setLayoutX(190);
            searchButton2.setLayoutY(270);

            Button viewTicketsButton = new Button("View tickets");
            viewTicketsButton.setPrefWidth(100);
            viewTicketsButton.setLayoutX(25);
            viewTicketsButton.setLayoutY(470);

            Button viewVouchersButton = new Button("View vouchers");
            viewVouchersButton.setPrefWidth(100);
            viewVouchersButton.setLayoutX(150);
            viewVouchersButton.setLayoutY(470);


            Text buyTicketText = new Text("Enter concert id to buy a ticket");
            buyTicketText.setX(25);
            buyTicketText.setY(330);

            TextField concertField = new TextField();
            concertField.setLayoutX(20);
            concertField.setLayoutY(350);

            Button buyButton = new Button("Buy");
            buyButton.setLayoutX(35);
            buyButton.setLayoutY(390);

            Button voucherButton = new Button("Use voucher");
            voucherButton.setLayoutX(90);
            voucherButton.setLayoutY(390);

            buyButton.setOnAction(event ->  DBUtil.getDbUtil().checkBalance(concertField.getText(), userName));
            voucherButton.setOnAction(event -> DBUtil.getDbUtil().checkVouchers(concertField.getText(), userName));
            topUpButton.setOnAction(event -> DBUtil.getDbUtil().topUpBalance(topUpField.getText(), userName));
            viewTicketsButton.setOnAction(event -> DBUtil.getDbUtil().printTickets(userName));
            viewVouchersButton.setOnAction(event -> DBUtil.getDbUtil().printVouchers(userName));
            searchButton.setOnAction(event -> DBUtil.getDbUtil().findConcert(searchField.getText()));
            searchButton2.setOnAction(event -> {
                try {
                    DBUtil.getDbUtil().searchInterval(searchField.getText(), searchField2.getText());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });

            userPane.getChildren().addAll(text, balanceText, refillPesetas, topUpField, topUpButton, searchText, dateFormat, searchField, searchField2, searchButton, searchButton2,
                    viewTicketsButton, viewVouchersButton, buyTicketText, concertField, buyButton, voucherButton);
        }


        //***********Getters and setters*****************

        public static Scene getUserScene() {
            return userScene;
        }

        public static view.UserView getUserView(String username) {
            if(userView == null){
                userView = new UserView(username);
            }
            return userView;
        }

        public static AnchorPane getUserPane() {
            return userPane;
        }
    }


