package view;

import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ViewManager {
    private static ViewManager view;
    private Scene mainScene;
    private Stage mainStage;
    private IState state;

    private ViewManager(Stage stage){
        state = LoginView.getLoginView();
        mainScene = LoginView.getLoginScene();
        mainStage = stage;
        mainStage.setTitle("Piljetter");
        mainStage.setScene(mainScene);
        mainStage.sizeToScene();
        mainStage.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, windowEvent ->{
            windowEvent.consume();
            System.exit(0);
        } );
        stage.show();
    }

    public static ViewManager getViewManager(Stage stage) {
        if(view == null){
            view = new ViewManager(stage);
        }
        return view;
    }

    public void setMainScene(Scene mainScene) {
        this.mainScene = mainScene;
        mainStage.setScene(mainScene);
    }
}